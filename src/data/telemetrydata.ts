/** Alert type */
export interface Alert {
  satelliteId: number;
  timestamp: Date;
  severity: string;
  component: string;
}

/** Telemetry data type */
export interface TelemetryData {
  timestamp: Date;
  satelliteId: number;
  redHighLimit: number;
  yellowHighLimit: number;
  yellowLowLimit: number;
  redLowLimit: number;
  rawValue: number;
  component: string;
}
