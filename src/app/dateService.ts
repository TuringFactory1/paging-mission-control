import { Alert, TelemetryData } from "../data/telemetrydata";

/**
 * Takes in a string date of yyyymmdd hh:mm:ss:msmsms, creates a Date object and returns it
 * @param dateString
 * @returns parsed and formatted date
 */
export function FormatDate(dateString: string) {
  let year = dateString.substring(0, 4);
  let month = dateString.substring(4, 6);
  let day = dateString.substring(6, 8);
  let hour = dateString.substring(9, 11);
  let minute = dateString.substring(12, 14);
  let seconds = dateString.substring(15, 17);
  let ms = dateString.substring(17, 21);
  let date = new Date(
    Date.UTC(+year, +month - 1, +day, +hour, +minute, +seconds, +ms)
  );
  return date;
}
/** This function sends an alert if there are at least 3 voltage readings
 *  that are under low red limit within 5 minutes OR
 *  three readings exceed the red high limit within 5 minutes
 *
 * outputs the THREE alerts that happen within 5 minutes with satellite id, timestamp, severity level, and component
 *
 */
export function FindAlerts(data) {
  Object.keys(data).forEach((element) => {
    let satelliteData = data[element];
    // Lets sort the data in case it is out of order
    satelliteData.sort((a, b) => a.timestamp - b.timestamp);
    // Filter out the data that has no values that exceed the red high/low limits
    satelliteData = satelliteData.filter(
      (x: TelemetryData) =>
        Number(x.rawValue) > Number(x.redHighLimit) ||
        Number(x.rawValue) < Number(x.redLowLimit)
    );

    // If the remaining data has less than 3 elements, we return and check the next satellite
    if (satelliteData.length < 3) return;

    let lowStack: TelemetryData[] = [];
    let highStack: TelemetryData[] = [];
    let outputStack: Alert[] = [];

    // Go through each of the remaining elements
    satelliteData.forEach((element: TelemetryData) => {
      let date = new Date(element.timestamp);
      // get the 5 minutes before and after the element's timestamp
      let timeBefore = date.setMinutes(date.getMinutes() - 5);
      let timeAfter = date.setMinutes(date.getMinutes() + 10);

      if (Number(element.rawValue) < Number(element.redLowLimit)) {
        if (
          lowStack.length === 0 ||
          (lowStack[lowStack.length - 1].timestamp.getTime() >= timeBefore &&
            lowStack[lowStack.length - 1].timestamp.getTime() <= timeAfter)
        ) {
          // push element onto the LOW alert stack if it is empty or is within the time range of the existing element in the stack.
          lowStack.push(element);
          // If there are three items in the stack then push those three to the output stack.
          if (lowStack.length === 3) {
            lowStack.map((x) => {
              outputStack.push({
                satelliteId: x.satelliteId,
                timestamp: x.timestamp,
                severity: "RED LOW",
                component: x.component,
              });
            });
            lowStack = [];
          }
        } else {
          // if this element is not within the time range of the latest, remove what is in there and push the current item onto the stack
          lowStack.pop();
          lowStack.push(element);
        }
      }

      if (Number(element.rawValue) > Number(element.redHighLimit)) {
        if (
          highStack.length === 0 ||
          (highStack[highStack.length - 1].timestamp.getTime() >= timeBefore &&
            highStack[highStack.length - 1].timestamp.getTime() <= timeAfter)
        ) {
          // push element onto the HIGH alert stack if it is empty or is within the time range of the existing element in the stack.
          highStack.push(element);
          // If there are three items in the stack then push those three to the output stack.
          if (highStack.length === 3) {
            highStack.map((x) => {
              outputStack.push({
                satelliteId: x.satelliteId,
                timestamp: x.timestamp,
                severity: "RED HIGH",
                component: x.component,
              });
            });
            highStack = [];
          }
        } else {
          // if this element is not within the time range of the latest, remove what is in there and push the current item onto the stack
          highStack.pop();
          highStack.push(element);
        }
      }
    });
    // log the output stack
    console.log(outputStack);
  });
}
