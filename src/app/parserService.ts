import { TelemetryData } from "../data/telemetrydata";
import { FormatDate } from "./dateService";
var fs = require("fs");
const readLine = require("readline");

/**
 * Parses file specified and returns a key value map with the key being the satellite ID
 * @param path this arg is the path of the file
 */
export async function ParseFile(path: string) {
  const fileStream = fs.createReadStream(path);
  const rl = readLine.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });

  let TelemetryDataArray: TelemetryData[] = [];
  for await (const line of rl) {
    var split = line.split("|");
    //taking into consideration that file is correctly formatted
    let data: TelemetryData = {
      timestamp: FormatDate(split[0]),
      satelliteId: split[1],
      redHighLimit: split[2],
      yellowHighLimit: split[3],
      yellowLowLimit: split[4],
      redLowLimit: split[5],
      rawValue: split[6],
      component: split[7],
    };

    TelemetryDataArray.push(data);
  }
  let TelemetryDataMap = {};

  // Lets map the satellite IDs to a key value array
  for (const telemetrydata of TelemetryDataArray) {
    if (!TelemetryDataMap[telemetrydata.satelliteId]) {
      TelemetryDataMap[telemetrydata.satelliteId] = [telemetrydata];
    } else {
      TelemetryDataMap[telemetrydata.satelliteId].push(telemetrydata);
    }
  }
  return TelemetryDataMap;
}
