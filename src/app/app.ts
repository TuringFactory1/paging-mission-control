import { ParseFile } from "./parserService";
import { FindAlerts } from "./dateService";

/**
 * Prompts user for input until an exit code is received, if user inputs an invalid path, reprompts the user.
 */
export const StartProgram = async () => {
  console.log("Starting App....");
  const inquirer = require("inquirer");
  // continue to prompt the user for input
  while (true) {
    try {
      await inquirer
        .prompt([
          {
            type: "input",
            name: "filePath",
            message: "Please specify file path",
          },
        ])
        .then(async (answers) => {
          console.log("Path:", answers["filePath"]);
          await ParseFile(answers["filePath"]).then((res) => {
            FindAlerts(res);
          });
        });
    } catch (e) {
      console.log(
        "File was not found, please specify full file path including file ext (txt) \n"
      );
    }
  }
};
